﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using IO.Practiceware.MU.Library;

namespace ConsoleMUWebService
{
    public class Program
    {
        static void Main(string[] args)
        {
            DataTable oPatientEducation;
            
            com.iopracticeware.portal.Service1 oService = new com.iopracticeware.portal.Service1();
            //oPatientEducation = oService.GetPatientEducationDetails("1126", "DITKOFF", "JONATHAN", "20160101", "20161231", 39);

            oPatientEducation = MedflowMUWebClass.GetSecureMessageDetails("1126", "DITKOFF", "JONATHAN", "20160101", "20161231", 39);

            Console.WriteLine(" PATIENTLASTNAME".PadRight(10)
                + "PATIENTFIRSTNAME".PadRight(10)
                + "PATIENTDOB".PadRight(10)
                + "PATIENTGENDER".PadRight(10));
            Console.WriteLine("----------------------------------------------------------");
            for (int i = 1; i < oPatientEducation.Rows.Count; i++)
            {
                Console.WriteLine(oPatientEducation.Rows[i][0].ToString().PadRight(10) +
                    oPatientEducation.Rows[i][1].ToString().PadRight(10) +
                    oPatientEducation.Rows[i][2].ToString().PadRight(10) +
                     oPatientEducation.Rows[i][3].ToString().PadRight(10));
            }
            Console.WriteLine(oPatientEducation.Rows.Count.ToString());
            Console.ReadKey();

        }
    }
}
