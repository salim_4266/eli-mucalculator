//------------------------------------------------------------------------------
// <copyright file="CSSqlStoredProcedure.cs" company="Microsoft">
//     Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------
using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using Microsoft.SqlServer.Server;
using System.Collections.Generic;
using System.ComponentModel;


public partial class StoredProcedures
{
    [Microsoft.SqlServer.Server.SqlProcedure]
    public static void GetPatientEducationDetails(string Practice, string ProviderLastName, string ProviderFirstName, string StartDate, string EndDate, int ResourceIds)
    {
        try
        {
            SqlPipe sqlPipe = SqlContext.Pipe;
            DataTable result = IO.Practiceware.MU.Library.MedflowMUWebClass.GetPatientEducationDetails(Practice, ProviderLastName, ProviderFirstName, StartDate, EndDate, ResourceIds);

            List<SqlMetaData> OutputColumns = new List<SqlMetaData>(result.Columns.Count);
            foreach (DataColumn col in result.Columns)
            {
                SqlMetaData OutputColumn = new SqlMetaData(col.ColumnName, SqlDbType.VarChar, col.MaxLength);
                OutputColumns.Add(OutputColumn);
            }

            SqlDataRecord record = new SqlDataRecord(OutputColumns.ToArray());
            SqlContext.Pipe.SendResultsStart(record);

            foreach (DataRow row in result.Rows)
            {
                for (int col = 0; col < result.Columns.Count; col++)
                {
                    record.SetValue(col, row.ItemArray[col]);
                }
                SqlContext.Pipe.SendResultsRow(record);
            }

            SqlContext.Pipe.SendResultsEnd();

        }
        catch (Exception ex)
        {
            SqlContext.Pipe.Send(ex.Message);
        }
    }


    [Microsoft.SqlServer.Server.SqlProcedure]
    public static void GetSecureMessageDetails(string Practice, string ProviderLastName, string ProviderFirstName, string StartDate, string EndDate, int ResourceIds)
    {
        try
        {
            SqlPipe sqlPipe = SqlContext.Pipe;
            DataTable result = IO.Practiceware.MU.Library.MedflowMUWebClass.GetSecureMessageDetails(Practice, ProviderLastName, ProviderFirstName, StartDate, EndDate, ResourceIds);

            List<SqlMetaData> OutputColumns = new List<SqlMetaData>(result.Columns.Count);
            foreach (DataColumn col in result.Columns)
            {
                SqlMetaData OutputColumn = new SqlMetaData(col.ColumnName, SqlDbType.VarChar, col.MaxLength);
                OutputColumns.Add(OutputColumn);
            }

            SqlDataRecord record = new SqlDataRecord(OutputColumns.ToArray());
            SqlContext.Pipe.SendResultsStart(record);

            foreach (DataRow row in result.Rows)
            {
                for (int col = 0; col < result.Columns.Count; col++)
                {
                    record.SetValue(col, row.ItemArray[col]);
                }
                SqlContext.Pipe.SendResultsRow(record);
            }

            SqlContext.Pipe.SendResultsEnd();

        }
        catch (Exception ex)
        {
            SqlContext.Pipe.Send(ex.Message);
        }
    }

   
    [Microsoft.SqlServer.Server.SqlProcedure]
    public static void GetVDTDetails(string Practice, string StartDate, string EndDate, int ResourceIds)
    {
        try
        {
            SqlPipe sqlPipe = SqlContext.Pipe;
            DataTable result = IO.Practiceware.MU.Library.MedflowMUWebClass.GetVDTDetails(Practice, StartDate, EndDate, ResourceIds);

            List<SqlMetaData> OutputColumns = new List<SqlMetaData>(result.Columns.Count);
            foreach (DataColumn col in result.Columns)
            {
                SqlMetaData OutputColumn = new SqlMetaData(col.ColumnName, SqlDbType.VarChar, col.MaxLength);
                OutputColumns.Add(OutputColumn);
            }

            SqlDataRecord record = new SqlDataRecord(OutputColumns.ToArray());
            SqlContext.Pipe.SendResultsStart(record);

            foreach (DataRow row in result.Rows)
            {
                for (int col = 0; col < result.Columns.Count; col++)
                {
                    record.SetValue(col, row.ItemArray[col]);
                }
                SqlContext.Pipe.SendResultsRow(record);
            }

            SqlContext.Pipe.SendResultsEnd();

        }
        catch (Exception ex)
        {
            SqlContext.Pipe.Send(ex.Message);
        }
    }

}
