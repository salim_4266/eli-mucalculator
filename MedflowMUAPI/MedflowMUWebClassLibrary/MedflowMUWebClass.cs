﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;


namespace IO.Practiceware.MU.Library
{
    public class MedflowMUWebClass
    {
        public static DataTable GetPatientEducationDetails(string Practice, string ProviderLastName, string ProviderFirstName, string StartDate, string EndDate, int ResourceIds)
        {
            DataTable oPatientEducation = null;
            try
            {
                com.iopracticeware.portal.Service1 oService = new com.iopracticeware.portal.Service1();
                oPatientEducation = oService.GetPatientEducationDetails(Practice, ProviderLastName, ProviderFirstName, StartDate, EndDate, ResourceIds);
            }
            catch
            {
                throw ;
            }
            return oPatientEducation;
        }

        public static DataTable GetSecureMessageDetails(string Practice, string ProviderLastName, string ProviderFirstName, string StartDate, string EndDate, int ResourceIds)
        {
            DataTable oSecureMessage = null;
            try
            {
                com.iopracticeware.portal.Service1 oService = new com.iopracticeware.portal.Service1();
                oSecureMessage = oService.GetSecureMessageDetails(Practice, ProviderLastName, ProviderFirstName, StartDate, EndDate, ResourceIds);
            }
            catch 
            {
                throw ;
            }
            return oSecureMessage;
        }

        public static DataTable GetVDTDetails(string Practice, string StartDate, string EndDate, int ResourceIds)
        {
            DataTable oVDTDetails = null;
            try
            {
                com.iopracticeware.portal.Service1 oService = new com.iopracticeware.portal.Service1();
                oVDTDetails = oService.GetVDTDetails(Practice, StartDate, EndDate, ResourceIds);
            }
            catch 
            {
                throw ;
            }
            return oVDTDetails;
        }
    }
}
