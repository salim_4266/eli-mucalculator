﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Services;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace MedflowMUWebService
{
    /// <summary>
    /// Summary description for Service1
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    public class Service1 : System.Web.Services.WebService
    {
        [WebMethod]
        public DataTable GetPatientEducationDetails(string Practice, string ProviderLastName, string ProviderFirstName, string StartDate, string EndDate, int ResourceIds)
        {
            DataTable datatableMU = null;
            try
            {


                string ConString = ConfigurationManager.ConnectionStrings[Practice].ConnectionString;
                SqlConnection connectionMU;
                SqlCommand commandMU = new SqlCommand();
                SqlDataAdapter dataadapterMU;

                using (connectionMU = new SqlConnection(ConString))
                {
                    commandMU.Connection = connectionMU;
                    commandMU.CommandType = CommandType.StoredProcedure;
                    commandMU.CommandText = Server.HtmlDecode("dbo.USP_GetPatientEducationDetails");

                    commandMU.Parameters.Add("@Practice", SqlDbType.VarChar).Value = Practice;
                    commandMU.Parameters.Add("@ProviderLastName", SqlDbType.VarChar).Value = ProviderLastName;
                    commandMU.Parameters.Add("@ProviderFirstName", SqlDbType.VarChar).Value = ProviderFirstName;
                    commandMU.Parameters.Add("@StartDate", SqlDbType.VarChar).Value = StartDate;
                    commandMU.Parameters.Add("@EndDate", SqlDbType.VarChar).Value = EndDate;
                    commandMU.Parameters.Add("@ResourceIds", SqlDbType.Int).Value = ResourceIds;

                    dataadapterMU = new SqlDataAdapter(commandMU);
                    datatableMU = new DataTable("PatientEducation");
                    dataadapterMU.Fill(datatableMU);
                }
            }
            catch(Exception ex)
            {
                throw ;
            }
            return datatableMU;
        }


        [WebMethod]
        public DataTable GetSecureMessageDetails(string Practice, string ProviderLastName, string ProviderFirstName, string StartDate, string EndDate, int ResourceIds)
        {
            DataTable datatableMU = null;
            try
            {

                string ConString = ConfigurationManager.ConnectionStrings[Practice].ConnectionString;
                SqlConnection connectionMU;
                SqlCommand commandMU = new SqlCommand();
                SqlDataAdapter dataadapterMU;

                using (connectionMU = new SqlConnection(ConString))
                {
                    commandMU.Connection = connectionMU;
                    commandMU.CommandType = CommandType.StoredProcedure;
                    commandMU.CommandText = "dbo.USP_GetSecureMessageDetails";

                    commandMU.Parameters.Add("@Practice", SqlDbType.VarChar).Value = Practice;
                    commandMU.Parameters.Add("@ProviderLastName", SqlDbType.VarChar).Value = ProviderLastName;
                    commandMU.Parameters.Add("@ProviderFirstName", SqlDbType.VarChar).Value = ProviderFirstName;
                    commandMU.Parameters.Add("@StartDate", SqlDbType.VarChar).Value = StartDate;
                    commandMU.Parameters.Add("@EndDate", SqlDbType.VarChar).Value = EndDate;
                    commandMU.Parameters.Add("@ResourceIds", SqlDbType.Int).Value = ResourceIds;

                    dataadapterMU = new SqlDataAdapter(commandMU);
                    datatableMU = new DataTable("SecureMessage");
                    dataadapterMU.Fill(datatableMU);
                }
            }
            catch (Exception ex)
            {
                throw ;
            }

            return datatableMU;
        }


        [WebMethod]
        public DataTable GetVDTDetails(string Practice, string StartDate, string EndDate,int ResourceIds)
        {
            DataTable datatableMU = null;
            try
            {
                string ConString = ConfigurationManager.ConnectionStrings[Practice].ConnectionString;
                SqlConnection connectionMU;
                SqlCommand commandMU = new SqlCommand();
                SqlDataAdapter dataadapterMU;

                using (connectionMU = new SqlConnection(ConString))
                {
                    commandMU.Connection = connectionMU;
                    commandMU.CommandType = CommandType.StoredProcedure;
                    commandMU.CommandText = "dbo.USP_GetVDTDetails";

                    commandMU.Parameters.Add("@Practice", SqlDbType.VarChar).Value = Practice;
                    commandMU.Parameters.Add("@StartDate", SqlDbType.VarChar).Value = StartDate;
                    commandMU.Parameters.Add("@EndDate", SqlDbType.VarChar).Value = EndDate;
                    commandMU.Parameters.Add("@ResourceIds", SqlDbType.Int).Value = ResourceIds;

                    dataadapterMU = new SqlDataAdapter(commandMU);
                    datatableMU = new DataTable("VDTDetails");
                    dataadapterMU.Fill(datatableMU);
                }
            }
            catch (Exception ex)
            {
                throw ;
            }

            return datatableMU;
        }
    }
}