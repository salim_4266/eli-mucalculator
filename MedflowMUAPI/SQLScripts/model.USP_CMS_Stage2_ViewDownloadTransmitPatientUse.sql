SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
ALTER PROCEDURE [model].[USP_CMS_Stage2_ViewDownloadTransmitPatientUse]    
(    
	@StartDate DateTime,    
	@EndDate DateTime,    
	@ResourceIds int    
)    
AS   

BEGIN    
	--MUST DEFINE PRACTICE NAME.
	DECLARE @Practice VARCHAR(200) 
	SELECT @Practice = Value FROM model.ApplicationSettings WHERE name = 'MedflowClientKey'  AND MachineName IS NULL AND USERID IS NULL

	IF @ResourceIds LIKE '%,%'
				RAISERROR ('Please ONLY send one doctor id at a TIME.',16	,2)

	DECLARE @TotParm INT;    
	DECLARE @CountParm INT;   

	SET @TotParm=0;    
	SET @CountParm=0;    

	--Denominator
	SELECT DISTINCT p.Id AS PatientId, p.FirstName, p.LastName, p.DateOfBirth, 
	SUBSTRING(g.Name,1,1) AS Gender, e.Id AS EncounterId, a.DateTime AS AppointmentDate
	INTO #Denominator
	FROM model.encounters e
	INNER JOIN model.appointments a ON a.EncounterId = e.id
	INNER JOIN model.patients p ON p.id = e.PatientId
	INNER JOIN model.Gender_Enumeration g ON g.Id = p.GenderId
	WHERE a.DateTime BETWEEN @StartDate and DATEADD(mi,1440,@EndDate) --DATEADD so that EndDate includes the whole day of encounters
	AND e.EncounterStatusId = 7
	AND a.UserId = @ResourceIds

	DECLARE @ProviderFirstName VARCHAR(200)
	DECLARE @ProviderLastname VARCHAR(200)
	SELECT @ProviderFirstName = ResourceFirstName, @ProviderLastname = ResourceLastName
	FROM Resources WHERE ResourceId = @ResourceIds

	CREATE TABLE #TmpPatientRepository (
	PATIENTREMOTEID VARCHAR(200) NULL,
	PATIENTLASTNAME VARCHAR(200) NULL,
	PATIENTFIRSTNAME VARCHAR(200) NULL,
	PATIENTDOB VARCHAR(200) NULL,
	PATIENTGENDER VARCHAR(200) NULL,
	ACTION VARCHAR(200) NULL,
	DATETIMEOFACTION VARCHAR(200) NULL
	)

	CREATE TABLE #BackupPatientRepository (
	PATIENTREMOTEID VARCHAR(200) NULL,
	PATIENTLASTNAME VARCHAR(200) NULL,
	PATIENTFIRSTNAME VARCHAR(200) NULL,
	PATIENTDOB VARCHAR(200) NULL,
	PATIENTGENDER VARCHAR(200) NULL,
	ACTION VARCHAR(200) NULL,
	DATETIMEOFACTION VARCHAR(200) NULL
	)

	INSERT INTO #TmpPatientRepository
	EXEC [dbo].[USP_GetVDTDetails] @Practice, @StartDate, @EndDate,@ResourceIds

	INSERT INTO #BackupPatientRepository
	SELECT DISTINCT OVDT.PATIENTREMOTEID,
	OVDT.PATIENTLASTNAME,
	OVDT.PATIENTFIRSTNAME,
	OVDT.PATIENTDOB,
	OVDT.PATIENTGENDER,
	OVDT.ACTION,
	OVDT.DATETIMEOFACTION FROM #TmpPatientRepository OVDT
	INNER JOIN model.Encounters ENC ON OVDT.PATIENTREMOTEID = ENC.PatientId
	INNER JOIN model.appointments AP ON AP.EncounterId = ENC.id
	WHERE OVDT.DATETIMEOFACTION BETWEEN @StartDate AND DATEADD(mi,1440,@EndDate)
	AND AP.UserId = @ResourceIds
	
	-- Numerator
	SELECT d.*
	INTO #Numerator
	FROM #Denominator d
	INNER JOIN #BackupPatientRepository bpr 
	ON bpr.PatientDob = d.DateOfBirth 
	AND bpr.patientFirstName = d.FirstName 
	AND bpr.patientLastName = d.LastName 
	AND bpr.patientGender = d.Gender

	SET @TotParm = (SELECT COUNT (DISTINCT PatientID) from #Denominator)
	SET @CountParm = (SELECT COUNT (DISTINCT PatientID) from #Numerator)

	SELECT @TotParm AS Denominator,@CountParm AS Numerator   
	SELECT d.PatientId, p.FirstName, p.LastName, d.EncounterId, d.AppointmentDate
	FROM #Denominator d
	INNER JOIN model.patients p ON p.id = d.PatientId
	ORDER BY p.LastName, p.FirstName, d.PatientId, d.AppointmentDate, d.EncounterId

	SELECT d.PatientId, p.FirstName, p.LastName, d.EncounterId, d.AppointmentDate
	FROM #Numerator d
	INNER JOIN model.patients p ON p.id = d.PatientId
	ORDER BY p.LastName, p.FirstName, d.PatientId, d.AppointmentDate, d.EncounterId

	DROP TABLE #TmpPatientRepository
	DROP TABLE #BackupPatientRepository
	DROP TABLE #Denominator
	DROP TABLE #Numerator
END 
SET ANSI_PADDING OFF
GO