SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
ALTER PROCEDURE [model].[USP_CMS_Stage2_SecureMessage]    
(    
	@StartDate DateTime,    
	@EndDate DateTime,    
	@ResourceIds int    
)    

AS   
BEGIN    
	
	--MUST DEFINE PRACTICE NAME.
	DECLARE @Practice VARCHAR(200) 
	SELECT @Practice = Value FROM model.ApplicationSettings WHERE name = 'MedflowClientKey'  AND MachineName IS NULL AND USERID IS NULL


	IF @ResourceIds LIKE '%,%'
				RAISERROR ('Please ONLY send one doctor id at a TIME.',16	,2)

	DECLARE @TotParm INT;    
	DECLARE @CountParm INT;   

	SET @TotParm=0;    
	SET @CountParm=0;    

	--Denominator
	SELECT PatientId, e.Id AS EncounterId, a.DateTime AS AppointmentDate
	INTO #Denominator
	FROM model.encounters e
	INNER JOIN model.appointments a ON a.EncounterId = e.id
	WHERE a.DateTime BETWEEN @StartDate AND DATEADD(mi,1440,@EndDate) --DATEADD so that EndDate includes the whole day of encounters
	AND e.EncounterStatusId = 7
	AND a.UserId = @ResourceIds

	DECLARE @ProviderFirstName VARCHAR(200)
	DECLARE @ProviderLastname VARCHAR(200)
	SELECT @ProviderFirstName = ResourceFirstName, @ProviderLastname = ResourceLastName
	FROM Resources WHERE ResourceId = @ResourceIds

	CREATE TABLE #BackupPatientRepository (
	PROVIDERREMOTEID varchar(200) NULL,
	PROVIDERLASTNAME varchar(200) NULL,
	PROVIDERFIRSTNAME varchar(200) NULL,
	PATIENTREMOTEID varchar(200) NULL,
	PATIENTLASTNAME varchar(200) NULL,
	PATIENTFIRSTNAME varchar(200) NULL,
	PATIENTDOB varchar(200) NULL,
	PATIENTGENDER varchar(200) NULL,
	DATETIMEOFACTION varchar(200) NULL
	)

	INSERT INTO #BackupPatientRepository
	EXEC [dbo].[USP_GetSecureMessageDetails] @Practice, @ProviderLastname, @ProviderFirstName, @StartDate, @EndDate, @ResourceIds

	--Numerator    
	SELECT d.*
	INTO #Numerator
	FROM #Denominator d
	INNER JOIN model.patients p ON p.id = d.PatientId
	INNER JOIN model.Gender_Enumeration g ON g.Id = p.GenderId
	INNER JOIN #BackupPatientRepository bpr 
	ON bpr.PatientDob = p.DateOfBirth 
	AND bpr.patientFirstName = p.FirstName 
	AND bpr.patientLastName = p.LastName 
	AND bpr.patientGender = SUBSTRING(g.Name,1,1)

	SET @TotParm = (SELECT COUNT (DISTINCT PatientID) FROM #Denominator)
	SET @CountParm = (SELECT COUNT (DISTINCT PatientID) FROM #Numerator)

	SELECT @TotParm AS Denominator,@CountParm AS Numerator   
	SELECT d.PatientId, p.FirstName, p.LastName, d.EncounterId, d.AppointmentDate
	FROM #Denominator d
	INNER JOIN model.patients p ON p.id = d.PatientId
	ORDER BY p.LastName, p.FirstName, d.PatientId, d.AppointmentDate, d.EncounterId

	SELECT d.PatientId, p.FirstName, p.LastName, d.EncounterId, d.AppointmentDate
	FROM #Numerator d
	INNER JOIN model.patients p on p.id = d.PatientId
	ORDER BY p.LastName, p.FirstName, d.PatientId, d.AppointmentDate, d.EncounterId

	DROP TABLE #BackupPatientRepository
	DROP TABLE #Denominator
	DROP TABLE #Numerator
END 
GO
SET ANSI_PADDING OFF
GO