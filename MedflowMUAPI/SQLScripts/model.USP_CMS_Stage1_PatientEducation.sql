SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO

ALTER PROCEDURE [model].[USP_CMS_Stage1_PatientEducation]    
(    
	@StartDate DateTime,    
	@EndDate DateTime,    
	@ResourceIds int    
)    

AS   
BEGIN    
	--MUST DEFINE PRACTICE NAME.
	DECLARE @Practice VARCHAR(200) 
	SELECT @Practice = Value FROM model.ApplicationSettings WHERE name = 'MedflowClientKey'  AND MachineName IS NULL AND USERID IS NULL



	DECLARE @TotParm INT;    
	DECLARE @CountParm INT;   

	SET @TotParm=0;    
	SET @CountParm=0;    

	--Initial Patient Population
	CREATE TABLE #InitialPatientPopulation (
	EncounterId INT,
	EncounterDate DATETIME,
	PatientId INT,
	DOB DATETIME,
	Age INT
	)

	INSERT INTO #InitialPatientPopulation
	EXEC model.USP_CMS_InitialPatientPopulation @StartDate,@EndDate,@ResourceIds

	--Denominator
	SELECT EncounterId, EncounterDate, PatientId
	INTO #Denominator
	FROM #InitialPatientPopulation ipp

	DECLARE @ProviderFirstName VARCHAR(200)
	DECLARE @ProviderLastname VARCHAR(200)
	SELECT @ProviderFirstName = ResourceFirstName, @ProviderLastname = ResourceLastName
	FROM Resources WHERE ResourceId = @ResourceIds

	CREATE TABLE #BackupPatientRepository (
	PROVIDERREMOTEID varchar(200) NULL,
	PROVIDERLASTNAME varchar(200) NULL,
	PROVIDERFIRSTNAME varchar(200) NULL,
	PATIENTREMOTEID varchar(200) NULL,
	PATIENTLASTNAME varchar(200) NULL,
	PATIENTFIRSTNAME varchar(200) NULL,
	PATIENTDOB varchar(200) NULL,
	PATIENTGENDER varchar(200) NULL,
	PATIENTEMAILID varchar(200) NULL,
	EMAILSUBJECT varchar(200) NULL,
	DATETIMEOFACTION varchar(200) NULL
	)

	INSERT INTO #BackupPatientRepository
	EXEC [dbo].[USP_GetPatientEducationDetails] @Practice, @ProviderLastname, @ProviderFirstName, @StartDate, @EndDate, @ResourceIds




	--Numerator 
	SELECT DISTINCT d.*
	INTO #Numerator
	FROM #Denominator d
	INNER JOIN model.patients p ON p.id = d.PatientId
	INNER JOIN model.Gender_Enumeration g ON g.Id = p.GenderId
	INNER JOIN #BackupPatientRepository bpr 
	ON bpr.PatientDob = p.DateOfBirth 
	AND bpr.patientFirstName = p.FirstName 
	AND bpr.patientLastName = p.LastName 
	AND bpr.patientGender = SUBSTRING(g.Name,1,1)
	
	UNION
	
	SELECT DISTINCT d.*
	FROM #Denominator d
	INNER  JOIN [Medflow].[ProcessedPatientEducationLinks] peLinks ON peLinks.EncounterId = d.EncounterId 
	AND peLinks.PatientId = d.PatientId
	INNER JOIN model.Encounters e ON e.Id = d.EncounterId 
	AND peLinks.EncounterId = d.EncounterId

	UNION
	SELECT DISTINCT d.*
	FROM #Denominator d
	INNER JOIN PatientClinical pc ON pc.PatientId = d.PatientId 
	AND pc.ClinicalType = 'a'
	WHERE pc.ClinicalType = 'A'
	AND pc.STATUS = 'A'
	AND (
	FindingDetail LIKE '%inform%' OR
	FindingDetail LIKE '%instructions%'
	)

	SELECT @TotParm = COUNT(DISTINCT PatientId) FROM #Denominator
	SELECT @CountParm = COUNT (DISTINCT PatientID) from #Numerator

	SELECT @TotParm AS Denominator,@CountParm AS Numerator   
	SELECT d.PatientId, p.FirstName, p.LastName, d.EncounterId, d.EncounterDate
	FROM #Denominator d
	INNER JOIN model.patients p ON p.id = d.PatientId
	ORDER BY p.LastName, p.FirstName, d.PatientId, d.EncounterDate, d.EncounterId

	SELECT d.PatientId, p.FirstName, p.LastName, d.EncounterId, d.EncounterDate
	FROM #Numerator d
	INNER JOIN model.patients p ON p.id = d.PatientId
	ORDER BY p.LastName, p.FirstName, d.PatientId, d.EncounterDate, d.EncounterId

	
	DROP TABLE #InitialPatientPopulation
	DROP TABLE #BackupPatientRepository
	DROP TABLE #Denominator
	DROP TABLE #Numerator
END

SET ANSI_PADDING OFF
GO